<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>TATA Motors</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-12 p-3">
            <img src="img/logos.png" class="img-fluid" alt=""/> 
        </div>
    </div>
    <div class="row mt-3 content">
        <div class="col-12 col-md-5 text-center">
            <img src="img/awards.png" class="img-fluid awards" alt=""/> 
        </div>
      <div class="col-12 col-md-5 offset-md-1">
            <div class="login">
                <form id="login-form" method="post" role="form">
                  <div id="login-message"></div>
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Name" aria-label="Name" aria-describedby="basic-addon1" name="name" id="name" required>
                  </div>
                  
                  <div class="input-group">
                    <input type="email" class="form-control" placeholder="Email" aria-label="Email" aria-describedby="basic-addon1" name="email" id="email" required>
                  </div>
                                    
                  <div class="input-group">
                    <button id="login" class="btn btn-primary btn-block login-button" type="submit">Login</button>
                  </div>
                
            </form>
            </div>
        
        </div>
    </div>
    
    
</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).on('submit', '#login-form', function()
{  
  $.post('chkforlogin.php', $(this).serialize(), function(data)
  {
     // console.log(data);
      if(data == 's')
      {
        window.location.href='webcast.php';  
      }
      else if (data == '-1')
      {
          $('#login-message').text('You are already logged in. Please logout and try again.');
          $('#login-message').addClass('alert-danger').fadeIn().delay(3000).fadeOut();
          return false;
      }
      else
      {
          $('#login-message').text(data);
          $('#login-message').addClass('alert-danger').fadeIn().delay(3000).fadeOut();
          return false;
      }
  });
  
  return false;
});
</script>
</body>
</html>